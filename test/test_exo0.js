import * as assert from "assert"
import { isTypedArray } from "util/types"
import {my_sum} from "../day-1/exercise-0.js"
describe('Exo 0',function(){
    describe('my_sum()',function(){
        it('sould return 3', function(){
            assert.equal(my_sum(2,1),3);
        });
        it('should return 0', function(){
            assert.equal(my_sum("a",1),0);
        });
        it('sould return 0', function (){
            assert.equal(my_sum(1),0);
        });
    });
});