import * as assert from "assert"
import { my_is_posi_neg } from "../day-1/exercise-7.js"

describe('Exo 7', function () {
    describe('my_is_posi_neg()', function () {
        it('should return POSITIF', function () {
            assert.equal(my_is_posi_neg(1), "POSITIF")
        });
        it('should return NEGATIF', function () {
            assert.equal(my_is_posi_neg(-2),"NEGATIF")
        });
        it('should return NEUTRAL', function () {
            assert.equal(my_is_posi_neg(0), "NEUTRAL")
        });
        it('should return POSITIF', function () {
            assert.equal(my_is_posi_neg(), "POSITIF")
        });
    });
});